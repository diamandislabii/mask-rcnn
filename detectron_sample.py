import json
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.colors as mcolors

from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.data.datasets import register_coco_instances
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import ColorMode, Visualizer
from detectron2.utils.memory import retry_if_cuda_oom

'''
Simple demonstration of using the model to perform segmentation on a single tile (128x128)
'''


class Detectron:
    def __init__(self):
        model_folder = './model'

        with open(os.path.join(model_folder, 'categories.json')) as f:
            ann_json_dat = json.load(f)

        register_coco_instances('dataset', {}, os.path.join(model_folder, 'categories.json'), '')

        cfg = get_cfg()

        cfg.merge_from_file(model_zoo.get_config_file('COCO-InstanceSegmentation/mask_rcnn_X_101_32x8d_FPN_3x.yaml'))
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = len(ann_json_dat['categories'])  # TODO: for now just 3
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7  # set a custom testing threshold
        cfg.MODEL.WEIGHTS = os.path.join(model_folder, 'model_final.pth')  # path to the model we just trained
        self.model = DefaultPredictor(cfg)

        self.classes = [x['name'] for x in ann_json_dat['categories']]  # TODO: for now just pos, neg, misc
        self.positive_idx = 0
        self.negative_idx = 1
        self.colormaps = [mcolors.to_rgba('r'), mcolors.to_rgba('b'), mcolors.to_rgba('g')]
        MetadataCatalog.get('dataset').set(thing_classes=self.classes, thing_colors=self.colormaps)
        self.metadata = MetadataCatalog.get('dataset')

    def run(self, tile):
        outputs = [retry_if_cuda_oom(self.model)(tile)]

        for i in range(len(outputs)):
            # visualizer uses rgb
            v = Visualizer(tile[:, :, ::-1], self.metadata, scale=1,
                           instance_mode=ColorMode.SEGMENTATION)
            instances = outputs[i]['instances'].to('cpu')

            if len(instances.get('pred_classes')):

                for k in range(len(instances)):
                    out = v.draw_binary_mask(np.array(instances.get('pred_masks')[k]),
                                             color=self.colormaps[instances.get('pred_classes')[k]])
                cam = out.get_image()  # it is RGB uint8 0~255
                # cam = cam[:, :, ::-1] # for cv2 use...uses bgr
                plt.imshow(cam)  # plt uses rgb
                plt.show()


if __name__ == '__main__':
    d = Detectron()
    tile_path = './path/to/tile'
    d.run(cv2.imread(tile_path))
