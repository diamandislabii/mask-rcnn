# README #

Part of "Compound computer vision workflow for efficient and automated immunohistochemical analysis of whole slide images".

https://doi.org/10.1136/jclinpath-2021-208020

Sample code to run the model on a single 128x128 tile is provided in 'detectron_sample.py'